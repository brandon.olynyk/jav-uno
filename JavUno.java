import java.util.InputMismatchException;
import java.util.Scanner;

public class JavUno {

	public static void main(String[] args) {
		lineBreak();
		Scanner scan = new Scanner(System.in);
		boolean mainLoop = true; // bool to continue main loop
		boolean roundLoop = false; // bool to continue the loop for a game's round
		boolean gameLoop = false; //bool to manage game loop (round and restarting game)
		final int MAX_PLAYERS = 6;
		Player[] initialPlayers = new Player[0]; // empty player's name info, kept for game restarts and set initially
		while (mainLoop) 
		{
			// main menu loop
			switch (mainMenu(scan)) {
				case 1:
					// start (manage players before switching loops)
					lineBreak();
					roundLoop = true;
					gameLoop = true;
					initialPlayers = managePlayers(scan, MAX_PLAYERS);
					Game game = new Game(initialPlayers); // reinitialize game (needed after another game is already done)
					while (gameLoop) {
						while (roundLoop) {
							roundLoop = doGame(game, scan);
						}
						if (!ask("Would you like to play again with the same people?", scan)) {
							gameLoop = false;
						} else {
							roundLoop = true; // restart round, reinit game
							game = new Game(initialPlayers);
						}
					}
					break;
				case 2: // help					
					printInstructions(scan);
					break;
				case 3: // leave					
					lineBreak();
					System.out.println("~ Goodbye ~\n");
					mainLoop = false;
					break;
			}
		}
	}

	public static boolean doGame(Game game, Scanner scan) {
		// game loop. Returns true if the game is still ongoing.
		if (game.doGameTurn()) {
			// winner declared, game done
			System.out.println("Congrats " + game.getWinningPlayer().getName() + ", you win!");
			return false;
		}
		return true;
	}

	public static Game resetGame(Player[] players) {
		return new Game(players); // reset game
	}

	public static Player[] resetPlayers(Player[] originalArray) {
		// given a Player[], re-initialize each player in the array with the same names
		// (but empty decks)
		Player[] resetArray = new Player[originalArray.length];
		for (int i = 0; i < resetArray.length; i++) {
			// for each player init w/ same name
			resetArray[i] = new Player(originalArray[i].getName());
		}

		return resetArray;
	}

	public static Player[] managePlayers(Scanner scan, int maxPlayers) {
		System.out.println("How many players would you like to have this game?\nMin 2, Max " + maxPlayers);
		boolean validInput = false;
		int playerCount = -1;
		while (!validInput) {
			// check if playerCount is valid (between 2 and 6)
			try {
				playerCount = scan.nextInt();
				if (playerCount >= 2 && playerCount <= maxPlayers) {
					validInput = true;
				} else {
					System.out.println("Invalid Player Count.");
				}
			} catch (InputMismatchException e) {
				System.out.println("Please enter a number.");
				scan = new Scanner(System.in); // re-init scanner
				// force the user to enter again
			}
		}
		lineBreak();
		Player[] players = new Player[playerCount]; // init player array
		for (int i = 0; i < playerCount; i++) {
			// init each player and their names
			System.out.println("What is Player " + (i + 1) + "'s name?");
			players[i] = new Player(scan.next());
		}
		// return when named all players
		lineBreak();
		return players;
	}

	public static boolean ask(String question, Scanner scan) {
		// asks the user a question and prompts them for a response
		boolean responseTaken = false; // turns true when a valid response is given
		boolean returnValue = false;

		while (!responseTaken) {
			// infinite loop, but broken once a value is returned
			System.out.print(question);
			System.out.println(" Enter 'Y' for yes, or 'N' for no.");
			String response = scan.next().toLowerCase();
			if (response.equals("y")) {
				responseTaken = true;
				returnValue = true;
			} else if (response.equals("n")) {
				responseTaken = true;
				returnValue = false;
			} else {
				System.out.println("Invalid answer!");
			}
		}
		return returnValue;
	}

	public static int mainMenu(Scanner scan) {
		// handles main menu, int returns choice, and the main loop takes it and decides
		// what to do with it
		//below is ascii bubble letter
		System.out.println("Welcome to...\n     ___  ________  ___      ___             ___  ___  ________   ________     \n     |\\  \\|\\   __  \\|\\  \\    /  /|           |\\  \\|\\  \\|\\   ___  \\|\\   __  \\    \n     \\ \\  \\ \\  \\|\\  \\ \\  \\  /  / /___________\\ \\  \\\\\\  \\ \\  \\\\ \\  \\ \\  \\|\\  \\   \n   __ \\ \\  \\ \\   __  \\ \\  \\/  / /\\____________\\ \\  \\\\\\  \\ \\  \\\\ \\  \\ \\  \\\\\\  \\  \n  |\\  \\\\_\\  \\ \\  \\ \\  \\ \\    / /\\|____________|\\ \\  \\\\\\  \\ \\  \\\\ \\  \\ \\  \\\\\\  \\ \n  \\ \\________\\ \\__\\ \\__\\ \\__/ /                 \\ \\_______\\ \\__\\\\ \\__\\ \\_______\\\n   \\|________|\\|__|\\|__|\\|__|/                   \\|_______|\\|__| \\|__|\\|_______|\n");
		System.out.println("\n-----TEXT INSIDE <> IS THE VALUE YOU NEED TO ENTER TO MAKE THAT SELECTION-----\n");
		System.out.println("<1>: Play");
		System.out.println("<2>: Instructions");
		System.out.println("<3>: Quit");

		int input = 0;
		boolean validInput = false;
		while (!validInput) {
			try {
				input = scan.nextInt();
				if (input > 0 && input <= 3) {
					validInput = true;
				} else {
					System.out.println("Invalid Input.");
				}
			} catch (InputMismatchException e) {
				System.out.println("Please enter a number.");
				scan = new Scanner(System.in);
				// force the user to enter again
			}
		}
		return input;
	}

	public static void printInstructions(Scanner scan) {
		// prints help text and asks player if they want to leave
		do {
			lineBreak();
			//help text
			System.out.println("-BASIC RULES-\n\nIn Uno, you play cards based on what card is in the top of the discard pile in an attempt\nto be the first one to get rid of all your cards. You can only play cards of the same\ncolor or value as the one in the discard pile. Each player\nplays one card before their turn is up.\n\n-INPUT-\n\nIn this game, you enter either numbers or letters, depending on what the game tells you to\nenter. In the game, when your deck is read out to you, cards will have an index value\non the leftmost side that looks like this:\n\n[0](RED - ONE)\n<1>(BLUE - FIVE)\n\nThe values inside [] cannot be played, while cards in <> can be played.\n\n-SPECIAL CARDS-\n\n- Black Cards: Can be played regardless of color or value.\n- Reverse Cards: Reverses the flow of the game. Instead of 1 -> 2 -> 3, it's 1 <- 2 <- 3.\n- Skip Cards: Can skip the next player's turn\n- PLUS2 and PLUS4 cards: Adds 2 or 4 cards to the next person's deck (plus stacking, explained next)\n\n-STACKING-\n\nHave some cards queued up for you from the last person's turn? Do not fret! Play another PLUS\ncard to avoid your responsibility and shove it to the next player! Your cards won't be added\nto your deck right away if you have a PLUS card to play, and will only be added to your deck\nif you don't play a PLUS card.");
		} while (!ask("\nDo you understand?", scan));
		lineBreak();
	}

	public static void lineBreak() {
		System.out
				.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}
}
