
import java.util.Scanner;
public class Game {

    //start of a game
    private boolean gameDone;
    //init game piles
    private Deck discardPile = new Deck();
    private Deck drawPile = new Deck();
    private Player[] players;
    private int winner = -1; // > 0 when a winner is declared, number corresponds to player (player1 = 1 etc)
    private Color forcedColor; //color to temporarily set the discard pile to, for black cards
    private boolean reversed = false; //when true, reverse turn order
    private int turnFlow = 1; //value added / subtracted to loop to determine turn order, either 1 or -1
    private int startingIndex = 0; //index to start at for the game loop. Default 0, reversed = players.length
    //bools to activate effects for the next turn's player
    boolean skip = false;
    int cardsToDraw = 0; //value declaring cards to draw for the next player, +2, +4 etc
    //utils
    private Scanner scan;
    //constants
    final int INITIAL_DRAWN_CARDS = 7; //cards drawn @ start of the game

    public Game(Player[] players) {
      this.gameDone = false;
      this.players = players;
      for (int i = 0; i < this.players.length; i++) {
        this.players[i].resetDeck(); // reset each player's deck (for restarts)
      }
      this.forcedColor = Color.BLACK; // black = don't force a color
      scan = new Scanner(System.in);
      initializeDecks(this.drawPile, this.discardPile);
    }

  public boolean doGameTurn()
  {
    //does the logic for a game turn for each player. Returns true if the game is done.
    for(int i=startingIndex; i < players.length && i >= 0; i += turnFlow)
    {
      System.out.println(players[i].getName() + "'s turn! Press enter to start your turn.");
      scan.nextLine(); //halts the program until enter is pressed, no peeking on other people's decks
      if(!skip)
      {
        checkForAddedCards(players[i]);       

        Card topCard = discardPile.getTop();
        String cardDisplay = setPrintedColor(topCard); //string to be printed, changes if a color is forced       
        System.out.println("The current card in the discard pile is " + cardDisplay);       
        System.out.println("----");

        Card playedCard = players[i].playTurn(discardPile, drawPile, forcedColor);
        discardPile.add(playedCard);
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        //after card played, reset tempColor to black (no change)
        this.forcedColor = Color.BLACK;
        checkForAddedCards(players[i]); //rechecks if there's cards to draw and player picked a normal card instead of a + card
        if (players[i].getDeck().length() == 0) //if no cards detected after turn ends, declare winner & break loop
        {
          winner = i; //winner = player i
          gameDone = true;
          break;
        }
          setNextTurnProperties(playedCard);
      }
      else
      {
        System.out.println("Your turn was skipped!");
        skip = false;
      }      
    }
    return gameDone;
  }

  public void checkForAddedCards(Player player)
  {
    //checks the player passed through if they have any plus cards. If not, add cardsToDraw number of cards
    //and reset cardsToDraw to 0.
    if(cardsToDraw > 0)
    {
      //add cards if cardstodraw > 0
      
      if(!player.plusCardExisits(discardPile.getTop())) //if playable plus card doesn't exist in player's deck, add queued cards if any
      {
        for(int j = 0; j < cardsToDraw; j++) //if cardstodraw != 0, draw that amount of cards.
        {
          player.draw(discardPile, drawPile);
        }
        cardsToDraw = 0;
      }
      else{
        System.out.println("You can play a plus card to stack to the next player.");
        System.out.println("You were sent " + this.cardsToDraw +" cards from a player before you! Play a PLUS card or take the extra cards instead.");
      }
    }
  }

  private String setPrintedColor(Card topCard) {
    // sets the string that displays the color of the current card in the discard pile.
    // if there's a forced color change due to a black card, change it to that.
    String cardDisplay = "";
    if (forcedColor == Color.BLACK) {
      // print card normally
      cardDisplay = "(" + topCard.toString() + ")"; // print card normally
    } else {
      // print card w/ forced color
      cardDisplay = ("(" + forcedColor + " - " + topCard.getValue() + ")");
    }
    return cardDisplay;
  }

  private void setNextTurnProperties(Card playedCard)
  {
    //sets properties for the next turn, such as + cards, skip, and forcing a card color value for wildcards
    if (playedCard.getColor() == Color.BLACK) {
      setWildCardColor();
    }

    if (playedCard.getValue() == Value.PLUS2) {
      this.cardsToDraw += 2;
    } else if (playedCard.getValue() == Value.PLUS4) {
      this.cardsToDraw += 4;
    } else if (playedCard.getValue() == Value.SKIP) {
      this.skip = true;
    } else if (playedCard.getValue() == Value.REVERSE) {
      toggleReverse();
    }
  }

  private void toggleReverse()
  {
    //toggles reverse and sets turnFlow accordingly
    reversed = !reversed;
    if (reversed) {
      turnFlow = -1;
      startingIndex = this.players.length - 1;
    } else {
      turnFlow = 1;
      startingIndex = 0;
    }
  }

  private void setWildCardColor()
  {
    //asks the user for a color to set the wildcard to, then sets forcedColor to that color to be used next turn
      System.out.println("You have activated a black card! Select the color you wish to set the card to: ");
      System.out.println("<G>: Green");
      System.out.println("<R>: Red");
      System.out.println("<Y>: Yellow");
      System.out.println("<B>: Blue");

      //asks the user a question and prompts them for a response
		  boolean responseTaken = false; //turns true when a valid response is given

      while (!responseTaken)
      {		
        //infinite loop, but broken once a value is returned         
          String response = scan.nextLine().toLowerCase();
          if(response.equals("g")){
            forcedColor = Color.GREEN;
            responseTaken = true;
          }
          else if(response.equals("r")){
            forcedColor = Color.RED;
            responseTaken = true;
          }
          else if(response.equals("b")){
            forcedColor = Color.BLUE;
            responseTaken = true;
          }
          else if(response.equals("y")){
            forcedColor = Color.YELLOW;
            responseTaken = true;
          }
          else
          {
            System.out.println("Invalid color!");
          }
      }
     	
  }

  public Player getWinningPlayer()
  {
    return players[winner];
  }

  private void initializeDecks(Deck drawPile, Deck discardPile)
	{ //fills the draw pile w/ all needed cards, and gives players and game decks their cards. used @ start of the game/	
		for(Color c : Color.values()){
			
			if(c != Color.BLACK)
			{
        for(Value v : Value.values())
        {
          if(v != Value.PLUS4 && v != Value.BLANK && v != Value.ZERO)
          {
            drawPile.add(new Card(c, v));
            drawPile.add(new Card(c, v));
          }
        }
				drawPile.add(new Card(c,Value.ZERO)); //only 1 zero card per color
			}
		}
    //add wildcard here
    drawPile.add(new Card(Color.BLACK, Value.PLUS4));
    drawPile.add(new Card(Color.BLACK, Value.PLUS4));
    drawPile.add(new Card(Color.BLACK, Value.BLANK));
    drawPile.add(new Card(Color.BLACK, Value.BLANK));
		drawPile.shuffle();
		for (int j = 0; j < players.length;j++) //give cards to players
		{
			for (int i = 0; i < INITIAL_DRAWN_CARDS; i++)
			{				
				drawPile.transfer(0, players[j].getDeck());
			}
		}
      drawPile.transfer(0, discardPile); //adds 1 card to the discard pile
	}

}
