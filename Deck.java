import java.util.Random;
public class Deck{
	
	//private Card[] cards
	
	private Card[] cardArray;
	private int deckSize;
	
	private Random random; //random obj set @ the start
	
	
	//constants
	final int SHUFFLE_COUNT = 242424; //how many times cards will be swapped when shuffled
	
	public Deck()
	{
		cardArray = new Card[1000];
		deckSize = 0;
		
		this.random = new Random();
	}
	
	public void add(Card c)
	{
		//adds string to the array at the end of the array
		this.cardArray[this.deckSize] = c;
		this.deckSize++;
	}
	
	public void remove(int i)
	{
		//removes string at index i, then downshifts all values of the array down, and sets size -= 1.
		for(int j = i; j < this.deckSize; j++)
		{
			this.cardArray[j] = this.cardArray[j + 1];
		}
		deckSize--;
	}
	
	public void insert(Card c, int i)
	{
		//inserts string s into index i after shifting all current values to the right, then increments the array size by 1
		for(int j = this.deckSize; j >= i; j--)
		{
			this.cardArray[j + 1] = this.cardArray[j];
		}
		deckSize++;
		//after shifting, set the index i to string s
		this.cardArray[i] = c;
		
	}
	
	public int length()
	{
		//returns length of array
		return deckSize;
	}
	
	public String toString()
	{
		//debug print, remove for shipping
		String builderString = "";
		for (int i=0; i < deckSize; i++)
		{
			 builderString += "<"+i+">("+cardArray[i]+")"+"\n";
		}
		return builderString;
	}

	public void print(boolean[] validCards)
	{
		//prints card based on bool[] input that describes if each card is playable. prints <> for playable and [] for not playable cards
		for (int i=0; i < deckSize; i++)
		{
			if(validCards[i])
			{
				System.out.println("<"+i+">("+cardArray[i]+")");
			}
			else
			{
				System.out.println("["+i+"]("+cardArray[i]+")");
			}
		}

	}
	
	public Card get(int i)
	{
		if (i >= deckSize)
		{
			return null;
			//index out of bounds, return null (not exception because we need to know if this is null)
		}
		else
		{
			return cardArray[i];
		}
	}

	public Card getTop()
	{
		//gets the topmost card, to make the discard pile reading easier
			return cardArray[deckSize - 1];
	}
	
	public void shuffle(){
		//shuffles the deck by swapping two indexes at random positions for a random length
		//random.nextInt() <- includes 0
		for(int i = 0; i < SHUFFLE_COUNT; i++)
		{
			//swap 2 random indexes
			swap(random.nextInt(deckSize), random.nextInt(deckSize));
		}
		
	}

	public void transfer(int index, Deck takerdeck){
		//gives a card from this deck @ position index to the takerdeck
		takerdeck.add(get(index));
		remove(index);
	}
	
	private void swap(int position1, int position2)
	{
		//swaps two indexes in the card array.
		Card tempCard = this.cardArray[position1];
		this.cardArray[position1] = this.cardArray[position2];
		this.cardArray[position2] = tempCard;
	}

public void takeAll(Deck takerDeck, boolean keepTop)
	{
		//transfers all the cards from 1 deck to another, except for the # of cards in the cardsToKeep int
		//for moving all cards to the discard pile back to the draw pile
		//taker deck = the deck to give all cards to
		int modifier = 1;
		if (keepTop)
		{
			modifier = 2; //makes it skip the last card in the upcoming loop
		}

		for(int i = deckSize-modifier; i >= 0; i--)
		{
			//transfer each card @i from donorDeck to this deck
			this.transfer(i, takerDeck);
		}
	}

	public void sort()
	{
		// sorts deck by color
		int cardsSorted = 0;
		for (Color c : Color.values()) {
			if (c != Color.BLACK) // needed to exclude black from sort and make it be last in the deck
			{
				// for each color take it and put it @ the start of the deck + cardsSorted
				for (int i = 0; i < this.deckSize; i++) {
					// for each card check if it's the current color
					if (cardArray[i].getColor() == c) {
						swap(cardsSorted, i);
						cardsSorted++;
					}
				}
			}
		}
	}
}
